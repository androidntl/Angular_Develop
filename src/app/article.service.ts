import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { Property } from './propertyform/property';
import { CabSearch } from './propertyform/cabSearch';
import { Article } from './article';
import { Greeting } from './greeting';


@Injectable()
export class ArticleService {
    //URLs for CRUD operations
  baseUrl="http://localhost:8888/";
  allArticlesUrl = this.baseUrl+"user/all-articles";
	articleUrl = this.baseUrl+"user/article";
  ruleUrl = this.baseUrl+"properties/newrule";
  allotUrl =this.baseUrl+ "cab/caballot";
  droolUrl = this.baseUrl+"cab/droolsTest";
	loginUrl = this.baseUrl+"user/login";
	getusers = this.baseUrl+"user/getAllUsers";
	statusCode: number;
	//Create constructor to get Http instance
	constructor(private http:Http) { 
	}
	//Fetch all articles
   getAllLogin(): Observable<Greeting[]> {
        return this.http.get(this.getusers)
		   		.map(this.extractData)
		        .catch(this.handleError);

    }
	register(greeting: Greeting): Observable<any>
	{
		console.log("get login url ",this.loginUrl);
		let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let cpParams = new URLSearchParams();
		//cpParams.set('email_id', email_id);
		//cpParams.set('pwd', pwd);
		console.log("services data ",greeting);
		console.log("this.loginUrl"+this.loginUrl);
		let options = new RequestOptions({ headers: cpHeaders});
		//console.log("Options ",options);
		return this.http.post(this.loginUrl,greeting,options)
		   		//.then(statusCode =>
				//{   console.log("get Response",statusCode.status);this.statusCode= statusCode.status;   });
		   		//.then(this.extractData)
		       // .catch(this.handleError);
		   		 .map(success => success.status);
		      //  .catch(this.handleError);;
	}
	//Create article
    createArticle(article: Article):Observable<number> {
	    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.articleUrl, article, options)
               .map(success => success.status)
               .catch(this.handleError);
    }
	//Fetch article by id
    getArticleById(articleId: string): Observable<Article> {
		let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
		let cpParams = new URLSearchParams();
		cpParams.set('id', articleId);			
		let options = new RequestOptions({ headers: cpHeaders, params: cpParams });
		return this.http.get(this.articleUrl, options)
			   .map(this.extractData)
			   .catch(this.handleError);
    }	
	//Update article
    updateArticle(article: Article):Observable<number> {
	    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.articleUrl, article, options)
               .map(success => success.status)
               .catch(this.handleError);
    }
    //Delete article	
    deleteArticleById(articleId: string): Observable<number> {
		let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
		let cpParams = new URLSearchParams();
		cpParams.set('id', articleId);			
		let options = new RequestOptions({ headers: cpHeaders, params: cpParams });
		return this.http.delete(this.articleUrl, options)
			   .map(success => success.status)
			   .catch(this.handleError);
    }		
	private extractData(res: Response) {
		console.log("result ",res);
	    let body = res.json();
        return body;
    }
    private handleError (error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.status);
    }

    //Create property
    createProperty(property: Property):Observable<number> {
   
    console.log('property called');
	    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.ruleUrl, property, options)
               .map(success => success.status)
               .catch(this.handleError);
    }

     //CabAllot
    cabAllot(cabSearch: CabSearch):Observable<number> {
   
    console.log('search called');
	    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.allotUrl, cabSearch, options)
               .map(success => success.status)
               .catch(this.handleError);
    }
}
