import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { ArticleComponent }  from './article.component';
import { LoginComponent }  from './login.component';
import { ArticleService } from './article.service';
import { LoginformComponent } from './loginform/loginform.component';
import { routes } from './app.router';
import { Routes, RouterModule, RouterOutlet } from '@angular/router';
import { PropertyformComponent } from './propertyform/propertyform.component';

@NgModule({
  imports: [     
        BrowserModule,
		HttpModule,
      ReactiveFormsModule, routes
  ],
  declarations: [
        AppComponent,
		ArticleComponent,LoginComponent, LoginformComponent, PropertyformComponent
  ],
  providers: [
        ArticleService
  ],
  bootstrap: [
        AppComponent
  ]
})
export class AppModule { }
