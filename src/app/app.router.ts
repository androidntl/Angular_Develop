import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule,RouterOutlet } from '@angular/router';
//import { RouterConfig } from '@angular/router';  

import { AppComponent } from './app.component';

import { LoginformComponent } from './loginform/loginform.component';
import { LoginComponent } from './login.component';
import { PropertyformComponent } from './propertyform/propertyform.component';
//import { canActivate } from '@angular/router';

export const router: Routes = [
    { path: '', component: LoginComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent,pathMatch: 'full' },
    { path: 'rule', component: PropertyformComponent,pathMatch: 'full' },
    { path: 'logins', component: LoginformComponent },
	//{ path:'**', redirectTo: '' }
    
    
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router, { enableTracing: true });
