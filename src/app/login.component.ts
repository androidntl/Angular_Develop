import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,FormBuilder,ReactiveFormsModule,FormControlName } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http  } from '@angular/http';
import { Observable } from "rxjs/rx";
import 'rxjs/Rx';
import { ArticleService } from './article.service';
import { Article } from './article';
import { Greeting } from './greeting';
declare var $;
@Component({
   selector: 'login-ctl',
   templateUrl: './login.component.html',
   //styleUrls: ['./article.component.css']
})
export class LoginComponent implements OnInit { 
   allGreetings: Greeting[];
   login: Article[];
	statusCode: number;
   dtOptions:{};
   name: string;
	 result: string;
		 public loginForm = new FormGroup({
		email: new FormControl("", Validators.required),
		password: new FormControl ("", Validators.required)
	  });
	  constructor(public http: Http,private articleService: ArticleService) {
		 
	  }
	   ngOnInit(): void {
	   this.getAllLogin();
	   setTimeout(function(){
	    $(function(){
	   $('#example').DataTable();
	   });
	   },3000);
	  
		
   }
   getAllLogin() {
        this.articleService.getAllLogin()
		  .subscribe(
                data => this.login = data,
                errorCode =>  this.statusCode = errorCode);   
   }
	  register() {
		//console.log(event);
		//this.result=this.loginForm.value;
		//console.log(this.loginForm.value);
		//let email_id=this.loginForm.controls['email'].value;
		//let pwd=this.loginForm.controls['password'].value;
		 let email = this.loginForm.get('email').value.trim();
		 let password = this.loginForm.get('password').value.trim();
		//alert(this.loginForm.controls['email'].value);
		//console.log(this.loginForm.controls['email'].value);
		let login_data= new Greeting(email, password);
		this.articleService.register(login_data)
		   .subscribe(
                successCode => { this.statusCode = successCode;
				    this.getAllLogin();
                errorCode =>  this.statusCode = errorCode
				}); 
		//console.log(JSON.stringify(this.loginForm.email.value));
		//console.log(this.loginForm.get('email').value);
	  } 
	  
  
}
