"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var loginform_component_1 = require("./loginform/loginform.component");
var login_component_1 = require("./login.component");
var rule_properies =require("./propertyform/propertyform.component");
//import { canActivate } from '@angular/router';
exports.router = [
    { path: '', component: login_component_1.LoginComponent, pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent, pathMatch: 'full' },
    { path: 'logins', component: loginform_component_1.LoginformComponent },
    { path: 'rule', component: rule_properies.RuleformComponent, pathMatch: 'full' },
];
exports.routes = router_1.RouterModule.forRoot(exports.router, { enableTracing: true });
//# sourceMappingURL=app.router.js.map
