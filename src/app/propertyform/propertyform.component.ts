import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,FormBuilder,ReactiveFormsModule,FormControlName } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http  } from '@angular/http';
import { Observable } from "rxjs/rx";
import 'rxjs/Rx';
import { ArticleService } from '../article.service';
import { Property } from './property';
import { CabSearch } from './cabsearch';
declare var $;

@Component({
  selector: 'app-propertyform',
  templateUrl: './propertyform.component.html',
  styleUrls: ['./propertyform.component.css']
})
export class PropertyformComponent implements OnInit {
    statusCode: number;
   constructor(public http: Http,private articleService: ArticleService) {
		 
	  }
   public ruleForm = new FormGroup({
		branch_id: new FormControl("", Validators.required),
		properties_name: new FormControl ("", Validators.required),
    properties_value: new FormControl("", Validators.required),
		propertie_category: new FormControl ("", Validators.required),
    cab_model: new FormControl("", Validators.required),
		cab_lat: new FormControl ("", Validators.required),
    cab_lng: new FormControl ("", Validators.required)
	  });

  ngOnInit() {
  }
  
    register() {   
    let branch_id = this.ruleForm.get('branch_id').value.trim();
    let properties_name = this.ruleForm.get('properties_name').value.trim();
    let properties_value = this.ruleForm.get('properties_value').value.trim();
    let propertie_category = this.ruleForm.get('propertie_category').value.trim();
    console.log(properties_name);
    let rule_data= new Property(branch_id, properties_name,properties_value,propertie_category);
	  
    this.articleService.createProperty(rule_data)
		   .subscribe(
                successCode => { this.statusCode = successCode;				   
                errorCode =>  this.statusCode = errorCode;
				});        
     }

     cabSearch(){
     console.log('cabSearch');
     let branch_id = '2';
     let cab_model = this.ruleForm.get('cab_model').value.trim();
     let cab_lat = this.ruleForm.get('cab_lat').value.trim();
     let cab_lng = this.ruleForm.get('cab_lng').value.trim();
     let search_data= new CabSearch(branch_id,cab_model,cab_lat,cab_lng);
     this.articleService.cabAllot(search_data)
		   .subscribe(
                successCode => { this.statusCode = successCode;				   
               errorCode =>  this.statusCode = errorCode;
				});
     }
}
