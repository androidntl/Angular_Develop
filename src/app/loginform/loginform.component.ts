import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../article';

declare var $;

@Component({
  selector: 'loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {
    login: Article[];
    statusCode: number;
    constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
      this.getAllLogin();
      setTimeout(function () {
          $(function () {
              $('#example').DataTable();
          });
      }, 1000);


  }
  getAllLogin() {
      this.articleService.getAllLogin()
          .subscribe(
          data => this.login = data,
          errorCode => this.statusCode = errorCode);
  }
}
